import {Component, OnDestroy, OnInit} from '@angular/core';
import { merge, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {untilDestroyed} from 'ngx-take-until-destroy';

@Component({
  selector: 'app-root',
  template: `
    <div class="container" fxLayout="column">
      <div fxLayout="row" fxLayoutAlign="center center" fxLayoutGap="5px">
        <button mat-raised-button color="primary" (click)="keyDown$.next(-1)"> 위로</button>
        <button mat-raised-button color="primary" (click)="keyUp$.next(1)">아래로 </button>
        <button mat-raised-button color="basic" (click)="keyLeft$.next(1)">느리게</button>
        <button mat-raised-button color="basic" (click)="keyRight$.next(-1)">빠르게</button>
        <button mat-raised-button color="warn" (click)="onStopOrResume()">시작/정지</button>
        <!--      <button mat-button color="primary" (click)="keyDown$.next()">Key Down</button>-->
      </div>
      <div class="image_slide" fxLayout="row" fxLayoutAlign="center center" fxLayoutGap="5px">
        <div class="image_title">Slide No: </div>
        <div class="image_no">{{slideNo}}</div>
      </div>
    </div>
  `,
  styles: [`
  .container {
    width: 500px;
    height: 500px;
    margin-top: 50px;
    margin-left: 50px;
  }
  .image_slide {
    margin-top: 50px;
  }
    .image_title {
      color: black;
      font-size: 24px;
    }
    .image_no {
      width: 60px;
      height: 60px;
      border-radius: 10px;
      background: black;
      color: yellow;
      padding-top: 15px;
      text-align: center;
      font-size: 32px;
    }
  `]
})
export class AppComponent implements OnInit, OnDestroy {
  keyDown$ = new Subject<number>();
  keyUp$ = new Subject<number>();
  keyLeft$ = new Subject();
  keyRight$ = new Subject();
  keySpace$ = new Subject<number>();
  interval$ = new Subject();
  //
  keyValue;
  oldState = 0;
  lastState = false;
  curIdx: any = 5;
  interval;
  speed = [300, 500, 700, 1000, 1500, 2000, 2500, 3000, 4000, 5000];
  slideNo = 0;
  constructor() { }

  ngOnInit() {
    this.newSpeed();
    /** 위로 아래로, 정지 시작의 이벤트 발생시 그 값을 저장함*/
    merge( this.keyUp$, this.keyDown$, this.keySpace$)
      .pipe( untilDestroyed(this),
        startWith(1)).subscribe( value => this.keyValue = value);
    //
    /** 느리게 빠르게 이벤트 발생시 그 값을 저장함*/
    merge( this.keyLeft$, this.keyRight$).pipe(
      untilDestroyed(this),
      startWith(1)
    ).subscribe(  _=> this.newInterval());
    //
    this.interval$.pipe(
      map( _ => this.keyValue ),
      untilDestroyed(this),
    ).subscribe( value => {
      this.slideNo = this.slideNo + value;
      if( this.slideNo < 0 ) this.slideNo = 0;
      console.log('result', value);
    })
  }
  /** 느리게 빠르게 이벤트 발생시 그에 맞는 슬라이드 이동 속도를 세팅함*/
  newInterval() {
    if ( this.interval ) clearInterval( this.interval);
    return this.interval = setInterval(() => { this.interval$.next(); }, this.speed[this.curIdx]);
  }
  /** 슬라이드 이동 속도 변경시, Defined  Array의 값을 적용함.*/
  newSpeed() {
    return merge( this.keyLeft$, this.keyRight$)
      .pipe(
        untilDestroyed(this),
        map( value => {
          this.curIdx = this.curIdx + value;
          if( this.curIdx >= this.speed.length) this.curIdx = this.speed.length - 1;
          if( this.curIdx < 0 ) this.curIdx = 0;
          console.log('interval', this.speed[this.curIdx]);
          // return this.speed[this.curIdx];

        }),
        startWith(1000)
      ).subscribe();
  }
  /** 슬라이드 이동중에 정지하였다가 다시 시작하는 경우 원래의 진행방향으로 다시 진행하도록 함*/
  onStopOrResume() {
    // console.log('this.currentState', this.currentState, this.lastState);
    this.lastState = !this.lastState;
    /** 정지중에 위로, 아래로 이벤트가 발생하는 경우는 "시작/정지"의 정지 상태에서 시작한 것 처럼 상태를 변경함.*/
    if( this.keyValue !== 0) this.lastState = true;
    //
    if( this.lastState ) {
      this.oldState = this.keyValue;
      this.keyValue = 0;
    } else {
      this.keyValue = this.oldState;
    }
    //
    this.keySpace$.next(this.keyValue);
  }
  ngOnDestroy(): void {
    if ( this.interval ) clearInterval( this.interval);
  }
}
